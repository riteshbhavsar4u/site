<?php
class ems_IssueController extends EmsX_Controller_EController
{
	
	

	public function indexAction()
	{ 
		$filter=$this->getRequest()->getParam('efilter');
		$userModel=new Model_ems_eusers;
		$userRow=$userModel->getActiveUser(Model_admins::getCurrentUserId());
		
		if(EmsX_Security_Privileges::isAdmin())
		{
			$efilter=(!empty($filter))?$filter:md5('to_be_assigned');
			$this->_redirect($this->_helper->url->url(array(
					'module'=>'ems',
					'controller'=>'Issue',
					'action'=>'mdashboard',
					'efilter'=>$efilter
				),null,true));
		}
		switch ($userRow['role_id'])
		{
			case Model_ems_eroles::$enumRoles['agent']:
				$efilter=(!empty($filter))?$filter:md5('inbox');
				$this->_redirect($this->_helper->url->url(array(
					'module'=>'ems',
					'controller'=>'Issue',
					'action'=>'adashboard',
					'efilter'=>$efilter
				),null,true));
			break;
			case Model_ems_eroles::$enumRoles['manager']:
			default:	
				$efilter=(!empty($filter))?$filter:md5('to_be_assigned');
				$this->_redirect($this->_helper->url->url(array(
					'module'=>'ems',
					'controller'=>'Issue',
					'action'=>'mdashboard',
					'efilter'=>$efilter
				),null,true));
			break;	
		}
	}
	
	public function mdashboardAction()
    {
    	$filter=$this->getRequest()->getParam('efilter');
   	 	if($this->_getParam('layout') === null) {
            $this->view->layout()->leftbar = 'issue/mleftbar.phtml';
			//$this->view->layout()->contentbar = 'view/contentbar.phtml';
        }
    	$mdl=new Model_ems_issues;
    	$mdl->displayListFields=array('selection','id','cust_email_addr','last_viewed_on','subject','status','assignee_to','pool_id','created_on');
    	
		$mdl->searchableList[]='date_from';
		$mdl->searchableList[]='date_to';
		$mdl->searchableList[]='assignee_to';
		#$mdl->searchableList[]='created_on';
		$mdl->searchableList[]='cust_email_addr';
		$mdl->searchableList[]='mailbox_id';
    	$mdl->sortableList[]='pool_id';
		
    	
    	$mdl->setDbDefinition('selection', 'type','selectionCheckbox');
    	$mdl->setDbDefinition('selection', 'title','#');
    	$mdl->setDbDefinition('selection', 'other',array('valueField'=>'id','name'=>'issues_ids[]','checkAllID'=>false,
    	'evalHtml'=>'return $Ehtml->hiddenField("issueMainIds[]",$this->arrData["id"],array("class"=>"issueMainIds"));'));//'issue_ids_parent'
    	$mdl->setDbDefinition('pool_id', 'title','Pool');
    	$mdl->setDbDefinition('subject', 'title','Subject');
    	$mdl->setDbDefinition('subject', 'advSearch',true);
    	$mdl->setDbDefinition('subject', 'type','text');
    	$mdl->setDbDefinition('subject', 'length',255);
		$mdl->setDbDefinition('date_from', 'title','From Date');
		$mdl->setDbDefinition('date_from', 'type','date');
		$mdl->setDbDefinition('date_to', 'title','To Date');
		$mdl->setDbDefinition('date_to', 'type','date');
		$mdl->setDbDefinition('cust_email_addr', 'title','Sent From');
		$mdl->setDbDefinition('cust_email_addr', 'type','editor');
		$mdl->setDbDefinition('cust_email_addr', 'advSearch',true);
		//$mdl->setDbDefinition('to_addr', 'title','Sent To');
		//$mdl->setDbDefinition('to_addr', 'type', 'text');
		$mdl->setDbDefinition('last_viewed_on', 'title','Opened Last');
    	$mdl->setDbDefinition('last_viewed_on', 'type','systemdatetime');
    	
    	$mdll=new Model_ems_issuethreads();
    	$mdlPoolUsers=new Model_ems_poolusers();
    	$mdlPool=new Model_ems_poolmaster();
    	$mdlViews=new Model_ems_issueviews();
    	$mdlMailbox=new Model_ems_mailboxmaster();
    	
    	$params = array_merge( array('recordCount' => 25),  $mdl->filterParams($this->_getAllParams()) );
    	$sql = $mdl->buildSearchSelect( $params );
    	$sql = $mdl->buildSearchSelect( $params, null, 'a' );
    	
    	$sql->setIntegrityCheck(false);
    	
    	if(array_key_exists($mdl->getFieldPrependKey(), $params)) {
            $pre = $params[$mdl->getFieldPrependKey()];
            $sentTo = $mdl->getFieldFromParamsEms($params, 'date_to', $pre);
            $sentFrom = $mdl->getFieldFromParamsEms($params, 'date_from', $pre);
            $fromAddr = $mdl->getFieldFromParamsEms($params, 'cust_email_addr', $pre);
            $toAddr = $mdl->getFieldFromParamsEms($params, 'mailbox_id', $pre);
            if($sentTo !== false AND $sentFrom!==false AND !empty($sentTo) AND !empty($sentFrom)) 
            {
            	$sql->where("DATE_FORMAT(a.created_on, '%Y-%m-%d') >= ?",  $sentFrom);
            	$sql->where("DATE_FORMAT(a.created_on, '%Y-%m-%d') <= ?",  $sentTo);
            }
            
            
        }
        
        $sql = $mdll->buildSearchSelect( $params, $sql, 'b');
        $sql = $mdlPoolUsers->buildSearchSelect( $params, $sql, 'c');
        $sql = $mdlPool->buildSearchSelect( $params, $sql, 'd');
        
        
        $sql->joinInner( array('f' => $mdlMailbox->info('name')), '(a.mailbox_id=f.id)', array('dept_id'));
        $sql->joinLeft( array('b' => $mdll->info('name')), '(a.id=b.issue_id AND a.current_thread_id=b.id)', array('subject','from_addr','to_addr'));
		$sql->joinLeft( array('c' => $mdlPoolUsers->info('name')), '(c.user_id=a.assignee_to AND c.pool_id=a.id)', array());
        $sql->joinLeft( array('d' => $mdlPool->info('name')), '(a.pool_id=d.id)', array('d.name AS pool_id','poolId'=>'id'));
        $sql->joinLeft( array('e' => $mdlViews->info('name')), '(a.id=e.issue_id AND a.last_view_id=e.id)', array('last_viewed_on'=>'created_on'));
		
		
        $pageTitle='Inbox';
		$displayAssignment=false;
			
	        switch ($filter)
	        {
	        	case md5('to_be_assigned'):
	        		$roleUserModel=new Model_ems_erolesusers;
	        		$userModel=new Model_ems_eusers();
	        		$sql->where('a.classification=?',Model_ems_issues::$enumClassifications['active']);
	        		$sql->where('(a.assignee_to=0 OR a.assignee_to IS NULL OR a.assignee_to=\'\' 
	        		OR a.assignee_to IN (SELECT id FROM '.$roleUserModel->info('name').' WHERE status=\''.Model_ems_erolesusers::$enumStatus['inactive'].'\') 
	        		OR a.assignee_to IN (SELECT id FROM '.$userModel->info('name').' WHERE role NOT IN (?))
	        		)',EmsX_Security_Privileges::getEmsAccesibleRoles());
	        		$filter='to_be_assigned';
	        	break;
	        	case md5('assigned'):
	        		$roleUserModel=new Model_ems_erolesusers;
	        		$sql->where('a.classification=?',Model_ems_issues::$enumClassifications['active']);
	        		#$sql->where('a.assignee_to=?',Model_admins::getCurrentUserId());
	        		$sql->where('a.status=?',Model_ems_issues::$enumStatus['assigned']);
	        		$filter='assigned';
	        	break;
	        	case md5('new_reply'):
	        		$sql->where('a.classification=?',Model_ems_issues::$enumClassifications['active']);
	        		$sql->where('a.status=?',Model_ems_issues::$enumStatus['new_reply']);
	        		$filter='new_reply';
	        	break;
	        	case md5('resolved_reply'):
	        		$sql->where('a.classification=?',Model_ems_issues::$enumClassifications['active']);
	        		$sql->where('a.status=?',Model_ems_issues::$enumStatus['resolved_reply']);
	        		$filter='resolved_reply';
	        	break;
	        	case md5('closed_reply'):
	        		$sql->where('a.classification=?',Model_ems_issues::$enumClassifications['active']);
	        		$sql->where('a.status=?',Model_ems_issues::$enumStatus['closed_reply']);
	        		$filter='closed_reply';
	        	break;
	        	case md5('await_reply'):
	        		$sql->where('a.classification=?',Model_ems_issues::$enumClassifications['active']);
	        		$sql->where('a.status=?',Model_ems_issues::$enumStatus['await_reply']);
	        		$filter='await_reply';
	        	break;
	        	case md5('resolved'):
	        		$sql->where('a.classification=?',Model_ems_issues::$enumClassifications['active']);
	        		$sql->where('a.status=?',Model_ems_issues::$enumStatus['resolved']);
	        		$filter='resolved';
	        	break;
	        	case md5('closed'):
	        		$sql->where('a.classification=?',Model_ems_issues::$enumClassifications['active']);
	        		$sql->where('a.status=?',Model_ems_issues::$enumStatus['closed']);
	        		$filter='closed';
	        	break;
	        	case md5('spam'):
	        		$sql->where('a.classification=?',Model_ems_issues::$enumClassifications['spam']);
	        		$filter='spam';
	        	break;
	        	case md5('all'):
	        	default:
	        		$sql->where('a.classification=?',Model_ems_issues::$enumClassifications['active']);	
	        		$filter='all';
	        	break;	
	        }
	        
	        $pageTitle=Model_ems_issues::$enumFilters[$filter]['label'];
	        $displayAssignment=Model_ems_issues::$enumFilters[$filter]['displayAssignment'];
	        
	        if($displayAssignment)
	        $displayAssignment=EmsX_Security_Privileges::isActionAllow('assignment');
	        
	        
        $eusersModel=new Model_ems_eusers();
        $poolModel=new Model_ems_poolmaster();
        $deptModel=new Model_ems_deptmaster();
        
     	$this->view->arrAssignee=$eusersModel->getAllAssignee();
     	$this->view->arrPools=$poolModel->getAllPools();
		$this->view->arrDepts=$deptModel->getAll();
		$this->view->arrDeptsPools=array();
		foreach ($this->view->arrDepts as $depts)
		{
			$this->view->arrDeptsPools[$depts['id']]=array('attrs'=>$depts,'pools'=>$poolModel->getAllPoolsByDept($depts['id']));
		}
     	
        $this->view->dataProvider=$mdl->fetchAll($sql);
        $this->view->pageTitle=$pageTitle;
        $this->view->displayAssignment=$displayAssignment;
        

    	$this->_helper->GenericActionOutputEms($mdl, 'searchadv3colview', $params)//searchbasic3colview
            ->renderToKey('search'); 

        $this->_helper->GenericActionOutputEms($mdl, 'listidviewlinkview', $this->view->dataProvider)
            ->renderToKey('list');
		           
        $this->_helper->GenericActionOutputEms($mdl, 'paginationhorizontalview', array())
            ->setParam('sql', $sql)
            ->setParam('formid', 'customersform')
            ->setParam('paginationLinkClass', 'btnclass blue')
            ->renderToKey('pagination');
        $r = $this->_helper->GenericActionOutput($mdl, 'listallview', array())
            ->setParam('dontShowNewButton', true)->renderToKey('alllist');
            ;
    	
    }
    
	public function adashboardAction()
    {
    	
    	$filter=$this->getRequest()->getParam('efilter');
   	 	if($this->_getParam('layout') === null) {
            $this->view->layout()->leftbar = 'issue/aleftbar.phtml';
        }
		
		
		$mdl=new Model_ems_issues;
    	$mdl->displayListFields=array('selection','id','cust_email_addr','subject','status','assignee_to','pool_id','created_on');
    	
		$mdl->searchableList[]='date_from';
		$mdl->searchableList[]='date_to';
		#$mdl->searchableList[]='created_on';
		$mdl->searchableList[]='cust_email_addr';
		$mdl->searchableList[]='to_addr';
    	$mdl->sortableList[]='pool_id';
    	
    	$mdl->setDbDefinition('selection', 'type','selectionCheckbox');
    	$mdl->setDbDefinition('selection', 'title','#');
    	$mdl->setDbDefinition('selection', 'other',array('valueField'=>'id','name'=>'issues_ids[]','checkAllID'=>false,
    	'evalHtml'=>'return $Ehtml->hiddenField("issueMainIds[]",$this->arrData["id"],array("class"=>"issueMainIds"));'));//'issue_ids_parent'
    	$mdl->setDbDefinition('pool_id', 'title','Pool');
    	$mdl->setDbDefinition('subject', 'title','Subject');
    	$mdl->setDbDefinition('subject', 'type','text');
    	$mdl->setDbDefinition('subject', 'advSearch',true);
    	$mdl->setDbDefinition('subject', 'length',255);
		$mdl->setDbDefinition('date_from', 'title','From');
		$mdl->setDbDefinition('date_from', 'type','date');
		$mdl->setDbDefinition('date_to', 'title','To');
		$mdl->setDbDefinition('date_to', 'type','date');
		$mdl->setDbDefinition('cust_email_addr', 'title','Sent From');
		$mdl->setDbDefinition('cust_email_addr', 'type','editor');
		$mdl->setDbDefinition('cust_email_addr', 'advSearch',true);
		$mdl->setDbDefinition('to_addr', 'title','Sent To');
		$mdl->setDbDefinition('to_addr', 'type','text');
		
    	$mdll=new Model_ems_issuethreads();
    	$mdlPoolUsers=new Model_ems_poolusers();
    	$mdlPool=new Model_ems_poolmaster();
    	
    	$params = array_merge( array('recordCount' => 25),  $mdl->filterParams($this->_getAllParams()) );
    	$sql = $mdl->buildSearchSelect( $params );
    	$sql = $mdl->buildSearchSelect( $params, null, 'a' );
    	
    	$sql->setIntegrityCheck(false);
    	
    	if(array_key_exists($mdl->getFieldPrependKey(), $params)) {
            $pre = $params[$mdl->getFieldPrependKey()];
            $sentTo = $mdl->getFieldFromParamsEms($params, 'date_to', $pre);
            $sentFrom = $mdl->getFieldFromParamsEms($params, 'date_from', $pre);
            if($sentTo !== false AND $sentFrom!==false AND !empty($sentTo) AND !empty($sentFrom)) 
            {
            	$sql->where("DATE_FORMAT(a.created_on, '%Y-%m-%d') >= ?",  $sentFrom);
            	$sql->where("DATE_FORMAT(a.created_on, '%Y-%m-%d') <= ?",  $sentTo);
            }
        }
        
        
        
        $sql = $mdll->buildSearchSelect( $params, $sql, 'b');
        $sql = $mdlPoolUsers->buildSearchSelect( $params, $sql, 'c');
        $sql = $mdlPool->buildSearchSelect( $params, $sql, 'd');
        
        $sql->joinInner( array('c' => $mdlPoolUsers->info('name')), '(c.user_id=a.assignee_to AND c.pool_id=a.id)', array());
        $sql->joinLeft( array('b' => $mdll->info('name')), '(a.id=b.issue_id AND a.current_thread_id=b.id)', array('subject','from_addr','to_addr'));
        $sql->joinLeft( array('d' => $mdlPool->info('name')), '(a.pool_id=d.id)', array('d.name AS pool_id','poolId'=>'id'));
		
        $sql->where('a.assignee_to=?',Model_admins::getCurrentUserId());
		
			
	        switch ($filter)
	        {
	        	case md5('inbox'):
	        		$sql->where('a.classification=?',Model_ems_issues::$enumClassifications['active']);
	        		$filter='inbox';
	        	break;
	        	case md5('new'):
	        		$sql->where('a.classification=?',Model_ems_issues::$enumClassifications['active']);
	        		$sql->where('a.status='.Model_ems_issues::$enumStatus['new'].' OR  a.status='.Model_ems_issues::$enumStatus['assigned']);
	        		$filter='new';
	        	break;
	        	case md5('new_reply'):
	        		$sql->where('a.classification=?',Model_ems_issues::$enumClassifications['active']);
	        		$sql->where('a.status=?',Model_ems_issues::$enumStatus['new_reply']);
	        		$filter='new_reply';
	        	break;
	        	case md5('resolved'):
	        		$sql->where('a.classification=?',Model_ems_issues::$enumClassifications['active']);
	        		$sql->where('a.status=?',Model_ems_issues::$enumStatus['resolved']);
	        		$filter='resolved';
	        	break;
	        	case md5('resolved_reply'):
	        		$sql->where('a.classification=?',Model_ems_issues::$enumClassifications['active']);
	        		$sql->where('a.status=?',Model_ems_issues::$enumStatus['resolved_reply']);
	        		$filter='resolved_reply';
	        	break;
	        	case md5('await_reply'):
	        		$sql->where('a.classification=?',Model_ems_issues::$enumClassifications['active']);
	        		$sql->where('a.status=?',Model_ems_issues::$enumStatus['await_reply']);
	        		$filter='await_reply';
	        	break;
	        	case md5('closed'):
	        		$sql->where('a.classification=?',Model_ems_issues::$enumClassifications['active']);
	        		$sql->where('a.status=?',Model_ems_issues::$enumStatus['closed']);
	        		$filter='closed';
	        	break;
	        	case md5('closed_reply'):
	        		$sql->where('a.classification=?',Model_ems_issues::$enumClassifications['active']);
	        		$sql->where('a.status=?',Model_ems_issues::$enumStatus['closed_reply']);
	        		$filter='closed_reply';
	        	break;
	        	case md5('spam'):
	        		$sql->where('a.classification=?',Model_ems_issues::$enumClassifications['spam']);
	        		$filter='spam';
	        	break;
	        	case md5('all'):
	        	default:
	        		$sql->where('a.classification=?',Model_ems_issues::$enumClassifications['active']);	
	        		$filter='all';
	        	break;	
	        }
	        
	    $pageTitle=Model_ems_issues::$enumFiltersAgent[$filter]['label'];
	        
        $eusersModel=new Model_ems_eusers();
        $poolModel=new Model_ems_poolmaster();
        
        $this->view->dataProvider=$mdl->fetchAll($sql);
        $this->view->pageTitle=$pageTitle;
        
    	$this->_helper->GenericActionOutputEms($mdl, 'searchadv3colview', $params)
            ->renderToKey('search'); 

        $this->_helper->GenericActionOutputEms($mdl, 'listidviewlinkview', $this->view->dataProvider)
            ->renderToKey('list');
		           
        $this->_helper->GenericActionOutputEms($mdl, 'paginationhorizontalview', array())
            ->setParam('sql', $sql)
            ->setParam('formid', 'customersform')
            ->setParam('paginationLinkClass', 'btnclass blue')
            ->renderToKey('pagination');
        $r = $this->_helper->GenericActionOutput($mdl, 'listallview', array())
            ->setParam('dontShowNewButton', true)->renderToKey('alllist');
            ;
    }
    
    public function composeAction()
    { 
    	
    	$userAgent = new Zend_Http_UserAgent();
		$device = $userAgent->getDevice();
		$browser = $device->getBrowser();
    	if(strtolower($browser)=='chrome')
    	{
    		//header('Content-Type: text/html; charset=utf-8');
    	}
    	$id=$this->getRequest()->getParam('id');
  		$issueModel=new Model_ems_issues();
  		$issueThread=new Model_ems_issuethreads();
  		$issueRow=$issueModel->getActiveIssue($id);
  		if(!$issueRow)
  		{
  			Throw new Zend_Exception('Issue has been deleted OR not available in system!'); 
  		}
  		
  		if(!EmsX_Security_Privileges::canComposeEmail($issueRow))
  		{
  			/*Check if current user is Agent then redirect him to view page*/
  			$userRow=EmsX_Security_Privileges::getUser();
  			switch ($userRow['role_id'])
  			{
  				case Model_ems_eroles::$enumRoles['agent']:
  					//$this->_forward('view', 'Issue', 'ems', array('id'=>$id));
  					$this->_redirect($this->_helper->url->url(array(
					'module'=>'ems',
					'controller'=>'Issue',
					'action'=>'view',
					'id'=>$id
				)));
					return;
  				break;	
  			}
  			Throw new Zend_Exception('This action cannot be executed!');
  		}
  		
  		$this->view->displaySpamToAgent=false;
  		$this->view->displayNoRespToAgent=false;
  		$userRow=EmsX_Security_Privileges::getUser();
  		switch ($userRow['role_id'])
  		{
  			case Model_ems_eroles::$enumRoles['agent']:
  				$this->view->displaySpamToAgent=(EmsX_Security_Privileges::isActionAllow('spam') AND EmsX_Security_Privileges::canSpam($issueRow));
  				$this->view->displayNoRespToAgent=(EmsX_Security_Privileges::isActionAllow('markResolve') AND EmsX_Security_Privileges::canMarkResolved($issueRow));
				//var_dump($this->view->displayNoRespToAgent);die;
  			break;	
  		}	
  		
  		$issueThreads=$issueThread->getAllThreadsExceptCurrent($id,$issueRow['current_thread_id']);

  		$eusersModel=new Model_ems_eusers();
        $poolModel=new Model_ems_poolmaster();
     	
        $poolName=(isset($issueRow['pool_id']))?$poolModel->getRecord($issueRow['pool_id'])->name:'';
        $assigneeName=(isset($issueRow['assignee_to']))?$eusersModel->getRecord($issueRow['assignee_to'])->name:'';
     	
        $this->view->displayAssignment=EmsX_Security_Privileges::isActionAllow('assignment');
  		$this->view->issueRow=$issueRow;
  		$this->view->issueThreads=$issueThreads;
  		$this->view->poolName=$poolName;
  		$this->view->assigneeName=$assigneeName;
  		$this->view->displayResolved=EmsX_Security_Privileges::canResolved($issueRow);
  		
  		$this->_helper->Eactivity->viewEmail($id);
  		$this->view->onlyEmailView=true;
  		$this->view->form = $form;
    }
    
    public function viewAction()
    {
  		$id=$this->getRequest()->getParam('id');
  		$fileId=$this->getRequest()->getParam(md5('file_id'));
  		
  		$issueModel=new Model_ems_issues();
  		$issueThread=new Model_ems_issuethreads();
  		$issueRow=$issueModel->getActiveIssue($id);
  		if(!$issueRow)
  		{
  			Throw new Zend_Exception('Issue has been deleted OR not available in system!'); 
  		}
  			
  		if(!EmsX_Security_Privileges::canView($issueRow))
  		{
  			Throw new Zend_Exception('This action cannot be executed!');
  		}
  		
    	if(isset($fileId))
  		{
  			/*File for download*/
  			$modelFiles=new Model_files;
  			$fileData=$modelFiles->getFileRecord($fileId);
  			 
  			if($fileData && $fileData['cdnUrl'])
  			{
                $mimeType = $this->_helper->Eactivity->mimeContentType($fileData['displayName']);
	  			header("Content-type: ".$mimeType);
				header("Content-Disposition: filename=\"".$fileData['displayName']."\"");
				file_get_contents($fileData['secureUrl']);
  			}
  			else 
  			{
  				Throw new Zend_Exception('Sorry, Attachment is not available!'); 
  			}	
  		}
  		
  		$issueThreads=$issueThread->getAllThreadsExceptCurrent($id,$issueRow['current_thread_id']);

  		$eusersModel=new Model_ems_eusers();
        $poolModel=new Model_ems_poolmaster();
        
        $this->view->displayAssignment=EmsX_Security_Privileges::isActionAllow('assignment');
        $this->view->canCompose=(EmsX_Security_Privileges::canComposeEmail($issueRow) AND EmsX_Security_Privileges::isActionAllow('compose'))?true:false;
        $this->view->canResolved=(EmsX_Security_Privileges::canMarkResolved($issueRow) AND EmsX_Security_Privileges::isActionAllow('markResolve'))?true:false;
        $this->view->arrAssignee=$eusersModel->getAllAssignee();
        if(intval($issueRow['assignee_to'])>0)
        {
        	/*If assignee is manager then fetch manager's info*/
        	 $managerData=$eusersModel->getRecord($issueRow['assignee_to']);
        	 $this->view->arrAssignee[]=array("id"=>$managerData->id,"name"=>$managerData->name,'pool_id'=>$issueRow['pool_id']);
        }
     	$this->view->arrPools=$poolModel->getAllValidPoolsByMailbox($issueRow['mailbox_id']);
  		$this->view->issueRow=$issueRow;
  		$this->view->issueThreads=$issueThreads;
  		
  		$this->_helper->Eactivity->viewEmail($id);
    }
    
    
    
	
}
?>